REM Usage: run.bat 1021taylor.jar

@ECHO OFF
set JDK=C:\bin\Java\jdk-11.0.1\bin\java.exe
set FX_LIB=C:\bin\Java\javafx-sdk-11\lib
set MODULES=javafx.controls,javafx.fxml,javafx.swing
%JDK% --module-path %FX_LIB% --add-modules=%MODULES% -jar %1
