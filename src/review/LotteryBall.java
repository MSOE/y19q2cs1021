package review;

import java.util.Random;

public class LotteryBall {
    private int currentValue;
    private final int maxValue;
    private static final Random generator = new Random();

    public LotteryBall(int maxValue) {
        this.maxValue = maxValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public int getCurrentValue() {
        return currentValue;
    }

    public int roll() {
        currentValue = generator.nextInt(maxValue) + 1;
        return currentValue;
    }

    public int roll2() {
        currentValue = (int)(Math.random()*maxValue) + 1;
        return currentValue;
    }
}
