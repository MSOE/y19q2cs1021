package review;

public class Driver {
    public static void main(String[] args) {
        CourseGrade grade = new CourseGrade();
        grade.setExam1(88);
        grade.addLabScore(62);
        grade.addLabScore(78);
        grade.addLabScore(88);
        grade.addLabScore(70);
        grade.addQuizScore(90);
        grade.addQuizScore(90);
        grade.addQuizScore(100);
        System.out.println(grade.getCourseAverage());
        System.out.println(grade.getCourseGrade());

    }
}
