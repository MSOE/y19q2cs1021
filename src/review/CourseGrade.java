package review;


import java.util.ArrayList;

public class CourseGrade {
    private double exam1;
    private double exam2;
    private double finalExam;
    private ArrayList<Double> quizScores;
    private ArrayList<Double> labScores;

    public double getExam1() {
        return exam1;
    }

    public void setExam1(double exam1) {
        this.exam1 = exam1;
    }

    public double getExam2() {
        return exam2;
    }

    public void setExam2(double exam2) {
        this.exam2 = exam2;
    }

    public double getFinalExam() {
        return finalExam;
    }

    public void setFinalExam(double finalExam) {
        this.finalExam = finalExam;
    }

    // Return -1 if invalid week
    public double getQuizScore(int weekNumber) {
        double score = -1;
        if(weekNumber>0 && weekNumber<=quizScores.size()) {
            score = quizScores.get(weekNumber-1);
        }
        return score;
    }

    public void addQuizScore(double score) {
        quizScores.add(score);
    }

    // Return -1 if invalid week
    // Ternary operator
    // A ? B : C
    //  If A is true, then above line becomes B
    //  If A is false, then above line becomes C
    public double getLabScore(int weekNumber) {
        return weekNumber>0 && labScores.size()>=weekNumber ? labScores.get(weekNumber-1) : -1;
    }

    public void addLabScore(double score) {
        labScores.add(score);
    }

    public CourseGrade() {
        exam1 = -1;
        exam2 = -1;
        finalExam = -1;
        quizScores = new ArrayList<>();
        labScores = new ArrayList<>();
    }

    // Should ignore any scores that are <0
    public double getCourseAverage() {
        double sum = 0;
        int numberOfScores = 0;
        if(getExamAverage()>=0) {
            sum += 3*getExamAverage();
            numberOfScores += 3;
        }
        if(getQuizAverage()>=0) {
            sum += getQuizAverage();
            ++numberOfScores;
        }
        if(getLabAverage()>=0) {
            sum += getLabAverage();
            ++numberOfScores;
        }
        return numberOfScores==0 ? -1 : sum/numberOfScores;
    }

    public String getCourseGrade() {
        String grade = "A";
        double average = getCourseAverage();
        if(average<93) {
            grade = "AB";
        }
        if(average<89) {
            grade = "B";
        }
        if(average<85) {
            grade = "BC";
        }
        if(average<81) {
            grade = "C";
        }
        if(average<77) {
            grade = "CD";
        }
        if(average<74) {
            grade = "D";
        }
        if(average<70) {
            grade = "F";
        }
        return grade;
    }

    // Need to return -1 if no lab scores yet
    public double getLabAverage() {
        double sum = 0;
        for(int i=0; i<labScores.size(); i++) {
            sum += labScores.get(i);
        }
        return labScores.size()==0 ? -1 : sum/labScores.size();
    }

    // Need to return -1 if no quiz scores yet
    // If at least 2 quiz scores are available drop the lowest
    public double getQuizAverage() {
        double sum = 0;
        for(int i=0; i<quizScores.size(); i++) {
            sum += quizScores.get(i);
        }
        return quizScores.size()==0 ? -1 : sum/quizScores.size();
    }

    // Ignore if score is -1
    public double getExamAverage() {
        double sum = 0;
        int numberOfScores = 0;
        if(exam1>=0) {
            sum += exam1;
            ++numberOfScores;
        }
        if(exam2>=0) {
            sum += exam2;
            ++numberOfScores;
        }
        if(finalExam>=0) {
            sum += finalExam;
            ++numberOfScores;
        }
        return numberOfScores==0 ? -1 : sum/numberOfScores;
    }
}
