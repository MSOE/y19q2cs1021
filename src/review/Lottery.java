package review;

public class Lottery {
    private LotteryBall[] balls;
    private int numBalls;
    private int[] winningNumbers;

    public Lottery(int[] ballMaxValues) {
        numBalls = ballMaxValues.length;
        balls = new LotteryBall[numBalls];
        for(int i=0; i<numBalls; i++) {
            balls[i] = new LotteryBall(ballMaxValues[i]);
        }
    }

    public LotteryTicket buyTicket() {
        int[] numbers = new int[numBalls];
        for(int i=0; i<numBalls; i++) {
            numbers[i] = balls[i].roll();
        }
        return new LotteryTicket(numbers);
    }

    public void drawWinningNumbers() {
        winningNumbers = new int[numBalls];
        for(int i=0; i<numBalls; i++) {
            winningNumbers[i] = balls[i].roll();
        }
    }

    public int countMatchingNumbers(LotteryTicket ticket) {
        int matches = 0;
        for(int i=0; i<numBalls; i++) {
            if(ticket.getNumber(i)==winningNumbers[i]) {
                matches++;
            }
        }
        return matches;
    }
}
