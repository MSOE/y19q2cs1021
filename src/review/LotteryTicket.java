package review;

public class LotteryTicket {
    private int[] numbers;

    public LotteryTicket(int[] numbers) {
        this.numbers = numbers;
    }

    public int numNumbers() {
        return numbers.length;
    }

    public int getNumber(int index) {
        return numbers[index];
    }
}
