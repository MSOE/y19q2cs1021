package review;

import java.util.ArrayList;
import java.util.Scanner;

public class LotterySimulator {
    public static void main(String[] args) {
        final int numBalls = 5;
        int[] maxValues = new int[numBalls];
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the maximum value for each of the five balls");
        for(int i=0; i<numBalls; i++) {
            maxValues[i] = in.nextInt();
        }
        System.out.println("How many customers will buy tickets");
        int numCustomers =  in.nextInt();
        Lottery lottery = new Lottery(maxValues);
        ArrayList<LotteryTicket> tickets = new ArrayList<>();
        for(int i=0; i<numCustomers; i++) {
            tickets.add(lottery.buyTicket());
        }
        int[] numWinners = new int[numBalls+1];
        lottery.drawWinningNumbers();
        for(LotteryTicket ticket : tickets) {
            int numMatches = lottery.countMatchingNumbers(ticket);
            numWinners[numMatches]++;
//            switch (numMatches) {
//                case 0:
//                    numWinners[0]++;
//                    break;
//                case 1:
//                    numWinners[1]++;
//                    break;
//                case 2:
//                    numWinners[2]++;
//                    break;
//                case 3:
//                    numWinners[3]++;
//                    break;
//                case 4:
//                    numWinners[4]++;
//                    break;
//                case 5:
//                    numWinners[5]++;
//                    break;
//            }
        }
        for(int i=0; i<numWinners.length; i++) {
            System.out.println("Tickets with " + i + " matches: " + numWinners[i]);
        }
    }
}
