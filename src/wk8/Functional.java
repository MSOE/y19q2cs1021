package wk8;

import wk2.Circle;
import wk2.Shape;
import wk2.Square;
import wk2.Triangle;

import java.util.Arrays;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.OptionalDouble;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Functional {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("a", "function", "that", "returns", "a", "function");

        //<editor-fold desc="Counting">
        long count = words.stream()
                .count();
        long unique = words.stream()
                .distinct()
                .count();

        System.out.println("Count: " + count + " Unique: " + unique);
        //</editor-fold>

        //<editor-fold desc="Filtering">
        Predicate<String> isShort = (word -> word!=null && word.length()<5);

        Object[] shortWords = words.stream()
                .filter(isShort)
                .toArray();

        for(Object word : shortWords) {
            System.out.print(word + " ");
        }
        System.out.println();
        //</editor-fold>

        //<editor-fold desc="Mapping">
        List<String> lengths = words.stream()
                .map(word -> word + ": " + word.length())
                .collect(Collectors.toList());

        System.out.println(lengths);
        //</editor-fold>

        //<editor-fold desc="Alternative Mapping">
        Function<String, String> toLength = (word -> word + ": " + word.length());
        lengths = words.stream()
                .map(toLength)
                .collect(Collectors.toList());

        System.out.println(lengths);
        //</editor-fold>

        //<editor-fold desc="Stream Statistics">
        int sum = words.stream()
                .mapToInt(w -> w.length())
                .sum();

        System.out.println("Sum of lengths of all strings: " + sum);
        //</editor-fold>

        //<editor-fold desc="ForEach">
        words.stream()
                .forEach(w -> System.out.println(w + ": " + w.length()));

        // Can do forEach directly on a List
        words.forEach(w -> System.out.println(w + ": " + w.length()));
        //</editor-fold>

        //<editor-fold desc="Quiz: count unique">
        // Ignoring the first 10 elements in a stream of Strings, wordStream,
        // write functional code to count the number of unique elements.
        Stream<String> wordStream = Stream.of( "1",  "2",  "3",  "4",  "5",  "6",  "7",  "8",  "9", "10",
                                              "11", "12", "13", "12", "15", "12", "17", "12", "19");
        long uniqueCount = wordStream.skip(10)
                .distinct()
                .count();
        System.out.println("Unique count: " + uniqueCount);
        //</editor-fold>

        //<editor-fold desc="Quiz: doubles to ints">
        // Take a stream of doubles, dStream, and create a list of integers, ints, that contains
        // the values from the stream rounded to the nearest integer. (Hint: Math.round())
        Stream<Double> dStream = Stream.of(2.2, 2.4, 2.6, 8.3, 8.8, 9.0);
        List<Integer> ints = dStream.map(num -> (int)Math.round(num))
                .collect(Collectors.toList());
        System.out.println(ints);
        //</editor-fold>

        //<editor-fold desc="Quiz: Only Evens">
        // Use functional programming techniques to take a list of integers, numbers, and create
        // another list of integers containing only the even integers from the original list.
        Stream<Integer> numbers = Stream.of(3, 8, 7, 2, 1, 9, 8);
        List<Integer> evens = numbers.filter(i -> i%2==0)
                .collect(Collectors.toList());

        System.out.println(evens);
        //</editor-fold>

        //<editor-fold desc="Quiz: Filter by suffix">
        // Use functional programming techniques to create a list of all the strings ending with "ppy"
        // found in a list of strings called originals. (Hint: endsWith() is a method in the String class)
        Stream.of("happy", "discussion" /* ... */, "slappy")
                .filter(word -> word.endsWith("ppy"))
                .forEach(System.out::println);
                //.forEach(word -> System.out.println(word));
        //</editor-fold>

        Stream<Shape> shapes = Stream.of(new Circle(), new Square(4), new Circle(), new Triangle(1, 5));
        //<editor-fold desc="Extract Circles">
        List<Circle> circles = shapes.filter(shape -> shape instanceof Circle)
                .map(shape -> (Circle)shape)
                .collect(Collectors.toList());
        //</editor-fold>

        //<editor-fold desc="Area of Shapes">
        shapes = Stream.of(new Circle(), new Square(4), new Circle(), new Triangle(1, 5));
        double totalArea = shapes.mapToDouble(Shape::getArea)
                .sum();

        System.out.println("Total area: " + totalArea);
        //</editor-fold>

        //<editor-fold desc="Average area of Shapes">
        shapes = Stream.of(new Circle(), new Square(4), new Circle(), new Triangle(1, 5));
        OptionalDouble averageArea = shapes.mapToDouble(Shape::getArea)
                .average();

        if(averageArea.isPresent()) {
            System.out.println("Average area: " + averageArea.getAsDouble());
        }
        //</editor-fold>

        //<editor-fold desc="Summary Statistics">
        shapes = Stream.of(new Circle(), new Square(4), new Circle(), new Triangle(1, 5));
        DoubleSummaryStatistics areaStats = shapes.mapToDouble(Shape::getArea)
                .summaryStatistics();

        System.out.println("Number of shapes: " + areaStats.getCount());
        System.out.println("Total area: " + areaStats.getSum());
        System.out.println("Average area: " + areaStats.getAverage());
        System.out.println("Maximum area: " + areaStats.getMax());
        System.out.println("Minimum area: " + areaStats.getMin());
        //</editor-fold>
    }
}
