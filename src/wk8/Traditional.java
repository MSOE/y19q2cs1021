package wk8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Traditional {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("a", "function", "that", "returns", "a", "function");

        long count = 0;
        for(String word : words) {
            ++count;
        }

        //<editor-fold desc="Unique">
        long unique = Math.min(words.size(), 1);
        for(int i=1; i<words.size(); ++i) {
            List<String> counted = words.subList(0, i);
            if(!counted.contains(words.get(i))) {
                ++unique;
            }
        }
        //</editor-fold>

        System.out.println("Count: " + count + " Unique: " + unique);

        //<editor-fold desc="Filtering">
        List<String> shortWords = new ArrayList<>();
        for(String word : words) {
            if(!shortWords.contains(word)) {
                shortWords.add(word);
            }
        }

        System.out.println(shortWords);
        //</editor-fold>
    }
}
