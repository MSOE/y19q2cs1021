package wk5;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

public class Spinner extends Application {
    public static final int HEIGHT = 200;
    public static final int WIDTH = 200;
    private int angle;
    private Button spinningButton;

    @Override
    public void start(Stage stage) {
        angle = 0;
        spinningButton = new Button("Click Me");
        spinningButton.setOnAction(actionEvent -> {
            angle = (angle+10)%360;
            spinningButton.setRotate(angle);
        });
        stage.setScene(new Scene(new StackPane(spinningButton), WIDTH, HEIGHT));
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
