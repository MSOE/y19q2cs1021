package wk5;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class BlurLabel extends Application {
    public static final int HEIGHT = 50;
    public static final int WIDTH = 200;
    private Label displayText;
    private Button toggleButton;

    @Override
    public void start(Stage stage) {
        displayText = new Label("This is a label that contains text");
        toggleButton = new Button("Blur");
        toggleButton.setOnAction(actionEvent -> {
            if(displayText.getEffect()==null) {
                // Set blur effect
                toggleButton.setText("De-blur");
                displayText.setEffect(new GaussianBlur());
            } else {
                // Remove blur effect
                toggleButton.setText("Blur");
                displayText.setEffect(null);
            }
        });

        Pane pane = new FlowPane();
        pane.getChildren().addAll(displayText, toggleButton);

        stage.setScene(new Scene(pane, WIDTH, HEIGHT));
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
