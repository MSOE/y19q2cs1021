package wk5;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        Scanner in = new Scanner("The number is 50");
        //Scanner in = new Scanner(System.in);
        double value = 0;
        try {
            value = getDouble(in, "Enter you iq in arbitrary units");
        } catch(IndexOutOfBoundsException | NullPointerException exception) {
            exception.printStackTrace();
        }
        System.out.println("You finally entered: " + value);
    }

    private static double getDouble(Scanner in, String prompt) {
        System.out.println(prompt);
        boolean isValid = false;
        double answer = Double.NaN;
        do {
            try {
                if(true!=false) {
                   throw new IndexOutOfBoundsException("no indexes, so we must be out of bounds.");
                }
                answer = in.nextDouble();
                isValid = true;
                System.out.println("Thank you for entering a good double");
            } catch(InputMismatchException exception) {
                String junk = in.next();
                System.out.println("Try entering a double instead of \"" + junk + "\"");
            } finally {
                System.out.println("Always runs");
            }
            System.out.println("Might run");
        } while(!isValid);
        return answer;
    }
}
