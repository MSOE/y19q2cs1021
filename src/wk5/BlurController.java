package wk5;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.input.MouseEvent;

public class BlurController {
    @FXML
    private Button hideButton;
    @FXML
    private Button toggleButton;
    @FXML
    private Label displayText;

    @FXML
    public void handleToggle(ActionEvent actionEvent) {
        if(displayText.getEffect()==null) {
            // Set blur effect
            toggleButton.setText("De-blur");
            displayText.setEffect(new GaussianBlur());
        } else {
            // Remove blur effect
            toggleButton.setText("Blur");
            displayText.setEffect(null);
        }
    }

    @FXML
    public void hideButton(ActionEvent actionEvent) {
        hideButton.setVisible(false);
    }

    @FXML
    public void handleOpacity(MouseEvent mouseEvent) {
        Button button = (Button)mouseEvent.getSource();
        button.setOpacity(0);
    }

    @FXML
    public void handleOpacity2(MouseEvent mouseEvent) {
        Button button = (Button)mouseEvent.getSource();
        button.setOpacity(1);
    }
}
