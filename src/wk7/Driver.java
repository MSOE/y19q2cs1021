package wk7;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.Date;
import java.util.Scanner;

import static java.nio.file.StandardOpenOption.APPEND;


public class Driver {
    public static void main(String[] args) {
        Path path = Path.of("stuff.bin");
        try {
            writeDouble(path, 4.642460068139439E21);
            System.out.println(readLine(path));
        } catch (NoSuchFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeByte(Path path, int data) throws NoSuchFileException, IOException {
        try (OutputStream out = Files.newOutputStream(path)) {
            out.write(data);
        }
    }

    public static byte readByte(Path path) throws IOException {
        try (InputStream in = Files.newInputStream(path)) {
            return (byte)in.read();
        }
    }

    public static void writeAnotherByte(Path path, int data) throws NoSuchFileException, IOException {
        try (OutputStream out = Files.newOutputStream(path, APPEND)) {
            out.write(data);
        }
    }

    public static void writeDouble(Path path, double data) throws IOException {
        try (OutputStream out = Files.newOutputStream(path);
             DataOutputStream dOut = new DataOutputStream(out)) {
            dOut.writeDouble(data);
        }
    }

    public static double readDouble(Path path) throws IOException {
        try (DataInputStream in = new DataInputStream(Files.newInputStream(path))) {
            return in.readDouble();
        }
    }

    public static void writeLine(Path path, String line) throws IOException {
        try (PrintWriter out = new PrintWriter(Files.newOutputStream(path))) {
            out.println(line);
        }
    }

    public static String readLine(Path path) throws IOException {
        try (Scanner in = new Scanner(Files.newInputStream(path))) {
            return in.nextLine();
        }
    }

    public static void writeDate(Path path, Date date) throws IOException {
        try (ObjectOutputStream out = new ObjectOutputStream(Files.newOutputStream(path))) {
            out.writeObject(date);
        }
    }

    public static Date readDate(Path path) throws ClassNotFoundException, IOException {
        try (ObjectInputStream in = new ObjectInputStream(Files.newInputStream(path))) {
            return (Date)in.readObject();
        }
    }
}
