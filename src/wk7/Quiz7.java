package wk7;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class Quiz7 {
    private static Complex[] NUMBERS = {new Complex(1),
                                        new Complex(-2.3, 5),
                                        new Complex(0, 3),
                                        new Complex(8, 8)};

    public static void main(String[] args) {
        activity3("complexies.bin");
        activity4("complexies.bin");
        activity7("complexies.txt");
        activity8("complexies.txt");
        activity9("complexiesO.bin");
        activity10("complexiesO.bin");
    }

    private static void activity9(String filename) {
        try (ObjectOutputStream out = new ObjectOutputStream(Files.newOutputStream(Path.of(filename)))) {
            for(Complex num : NUMBERS) {
                out.writeObject(num);
            }
        } catch (IOException e) {
            System.err.println("Error writing to "  + filename);
        }
    }

    // Write binary
    private static void activity3(String filename) {
        try (DataOutputStream out = new DataOutputStream(Files.newOutputStream(Path.of(filename)))) {
            for(Complex num : NUMBERS) {
                num.write(out);
            }
        } catch (IOException e) {
            System.err.println("Error writing to "  + filename);
        }
    }

    // Read binary
    private static void activity4(String filename) {
        try (DataInputStream in = new DataInputStream(Files.newInputStream(Path.of(filename)))) {
            Complex num1 = Complex.read(in);
            Complex num2 = Complex.read(in);
            System.out.println("Cart:");
            System.out.println(num1 + " and " + num2);
            System.out.println("Polar:");
            Complex.setPolar();
            System.out.println(num1 + " and " + num2);
        } catch (IOException e) {
            System.err.println("Error reading from " + filename);
        }
    }

    // Write text
    private static void activity7(String filename) {
        try (PrintWriter out = new PrintWriter(Files.newOutputStream(Path.of(filename)))) {
            for(Complex num : NUMBERS) {
                num.write(out);
            }
        } catch (IOException e) {
            System.err.println("Error writing to "  + filename);
        }
    }

    // Read text
    private static void activity8(String filename) {
        try (Scanner in = new Scanner(Files.newInputStream(Path.of(filename)))) {
            while(in.hasNextLine()) {
                Complex num = Complex.read(in);
                System.out.println(num);
            }
        } catch (IOException e) {
            System.err.println("Error reading from " + filename);
        }
    }

    // Write as Objects
    private static void activity10(String filename) {
        try (ObjectInputStream in = new ObjectInputStream(Files.newInputStream(Path.of(filename)))) {
            Complex.setPolar();
            for(int i=0; i<4; i++) {
                Complex num = (Complex)in.readObject();
                System.out.println(num);
            }
        } catch (IOException e) {
            System.err.println("Error reading from " + filename);
        } catch (ClassNotFoundException e) {
            System.err.println("No such class available");
        }
    }
}
