package wk6;

import java.io.DataInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class Driver {
    public static void main(String[] args) {
        Path stuff = Path.of("grades.xlsx");
        try (DataInputStream in = new DataInputStream(Files.newInputStream(stuff))) {
            System.out.println(in.readBoolean());
            System.out.println(in.readDouble());
            System.out.println(in.readDouble());
            System.out.println(in.readChar());
            System.out.println(in.readChar());
            System.out.println(in.readChar());
            System.out.println(in.readChar());
            System.out.println(in.readChar());
            System.out.println(in.readChar());
            System.out.println(in.readChar());
            System.out.println(in.readChar());
            System.out.println(in.readChar());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
