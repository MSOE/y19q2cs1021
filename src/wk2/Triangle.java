package wk2;

public class Triangle extends Shape {
    private double length;
    private double height;

    public Triangle(double length, double height) {
        super(null);
        this.length = length;
        this.height = height;
        System.out.println("Triangle: constructor");
    }

    public Triangle() {
        this(1, 1);
    }

    @Override
    public void draw() {
        System.out.println("Triangle: draw()");
    }

    @Override
    public double getArea() {
        return length*height/2;
    }
}
