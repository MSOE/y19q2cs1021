package wk2;

import javafx.scene.paint.Color;
import wk3.Movable;

import java.io.Serializable;

public class Square extends Shape implements Movable, Serializable {
    private double sideLength;

    public Square() {
        this(1);
        System.out.println("Square: no-arg constructor");
    }

    public void draw() {
        System.out.println("Square: draw");
    }

    @Override
    public double getArea() {
        return sideLength * sideLength;
    }

    public Square(double size) {
        super(Color.WHITE);
        sideLength = size;
        System.out.println("Square: 1-arg constructor");
    }

    @Override
    public boolean equals(Object that) {
        boolean equal = this==that;
        if(!equal && that instanceof Square) {
            Square sqr = (Square)that;
            equal = sideLength==sqr.sideLength;
        }
        return equal;
    }

    @Override
    public void zoom(double magnification) {
        super.zoom(magnification);
        System.out.println("Square: zoom");
        sideLength *= magnification;
    }

    @Override
    public void move(double x, double y) {
        this.xCoord += x;
        this.yCoord += y;
    }
}
