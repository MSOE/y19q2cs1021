package wk2;

import javafx.scene.paint.Color;

public class Circle extends Shape {
    private double radius;

    public Circle() {
        super(Color.GRAY);
        radius = 0.0;
        System.out.println("Circle: constructor");
    }

    public void draw() {
        System.out.println("Circle: draw");
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    public void erase() {
        System.out.println("Circle: erase");
        super.erase();
    }

    public void zoom(double magnification) {
        radius *= magnification;
        System.out.println("Circle: zoom");
    }

    public void setRadius(double radius) {
        this.radius = radius;
        System.out.println("Circle: setRadius");
    }

    public double getRadius() {
        System.out.println("Circle: getRadius");
        return radius;
    }
}
