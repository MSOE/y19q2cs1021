package wk2;

import javafx.scene.paint.Color;
import wk3.Movable;

import java.util.ArrayList;
import java.util.List;

public class Driver {
    public static void main(String[] args) {
        List<Shape> shapes = new ArrayList<>();
        for(int i=0; i<5; ++i) {
            shapes.add(Math.random()<0.5 ? new Circle() : new Square(2));
        }
        for(Shape shape : shapes) {
            shape.draw();
            if(shape instanceof Movable) {
                Movable m = (Movable) shape;
            }
        }
    }
}
