package wk2;

import javafx.scene.paint.Color;

public abstract class Shape {
    protected Color color;
    protected double xCoord;
    protected double yCoord;

    public static int instanceCount() {
        return 0;
    }

    public Shape(Color c) {
        color = c;
        xCoord = 0.0;
        yCoord = 0.0;
        System.out.println("Shape: constructor");
    }

    public abstract void draw();

    public abstract double getArea();

    public void erase() {
        System.out.println("Shape: erase");
    }

    public void move() {
        xCoord += 5.0;
        yCoord += 5.0;
        System.out.println("Shape: move");
    }

    public void zoom(double magnification) {
        System.out.println("Shape: zoom");
    }

    public Color getColor() {
        System.out.println("Shape: getColor");
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
        System.out.println("Shape: setColor");
    }

    public String toString() {
        return "I'm a " + color + " shape at (" + xCoord + ", " + yCoord + ")";
    }
}
