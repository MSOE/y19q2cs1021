package wk3;

public interface Movable {
    void move(double x, double y);
}
