### Implement `First`
``` java
public interface First {
    int iMethodA(int a);
    int iMethodB(int a);
}
```

### Show the class declaration for `Child` (inherits from `Parent` and implements `First`)
``` java
public class Child extends Parent implements First {
```

### Implement `Child` constructor
``` java
public Child() {
    super("");
}
```

### List all legal reference types for `XXX` where
``` java
XXX obj = new Child();
```

* `Child`
* `Object`
* `First`
* `Parent`
