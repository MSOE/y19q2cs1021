package wk3;

public class Integer extends Calculable {
    private final int value;

    public Integer(int value) {
        this.value = value;
    }

    @Override
    public Calculable plus(Calculable that) {
        return new Double(that.intValue() + value);
    }

    @Override
    public Calculable minus(Calculable that) {
        return new Double(that.intValue() - value);
    }

    @Override
    public double doubleValue() {
        return value;
    }

    @Override
    public int intValue() {
        return value;
    }
}
