package wk3;

public abstract class Calculable {
    public abstract Calculable plus(Calculable that);
    public abstract Calculable minus(Calculable that);
    public abstract double doubleValue();
    public abstract int intValue();
}
