package wk3;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

// Be sure to add appropriate VM options in Run -> Edit Configurations:
//  --module-path C:\bin\Java\javafx-sdk-11.0.1\lib --add-modules=javafx.controls,javafx.fxml
public class HelloWorld extends Application {

    private Circle circle;

    @Override
    public void start(Stage stage) {
        Group root = new Group();
        Scene scene = new Scene(root, 200, 150);
        scene.setFill(Color.LIGHTGRAY);

        circle = new Circle(60, 40, 30, Color.GREEN);

        Text text = new Text(10, 90, "JavaFX Scene");
        text.setFill(Color.DARKRED);
        Button bigger = new Button("Bigger");
        bigger.setOnAction(this::enlargeCircle);

        Font font = new Font(20);
        text.setFont(font);

        root.getChildren().add(circle);
        root.getChildren().add(text);
        root.getChildren().add(bigger);
        stage.setScene(scene);
        stage.show();
    }

    private void enlargeCircle(ActionEvent actionEvent) {
        circle.setRadius(circle.getRadius()*1.1);
    }

    public void start2(Stage stage) {
        Label message = new Label("Hello World! Today is a great day to be a world.");
        Scene scene = new Scene(message);

        stage.setTitle("Title bar");
        stage.setFullScreen(true);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
