package wk3;

public class Double extends Calculable {
    private final double value;

    public Double(double value) {
        this.value = value;
    }

    @Override
    public Calculable plus(Calculable that) {
        return new Double(that.doubleValue() + value);
    }

    @Override
    public Calculable minus(Calculable that) {
        return new Double(that.doubleValue() - value);
    }

    @Override
    public double doubleValue() {
        return value;
    }

    @Override
    public int intValue() {
        return (int)value;
    }
}
