package wk1;

public class Encryptor {

    public String encrypt(String phrase) {
        String result = "";
        // For each letter in phrase
        for(int i=0; i<phrase.length(); ++i) {
            //  if not letter > add to new string
            //  if is letter > shift 13 characters
            char character = phrase.charAt(i);
            if(Character.isLetter(character)) {
                result += rotateLetter(character);
            } else {
                result += character;
            }

        }
        return result;
    }

    private char rotateLetter(char letter) {
        // A ? B : C
        // If A is true, the expression becomes B
        // If A is false, the expression becomes C
        return (Character.toLowerCase(letter)<'a'+13)
                ? (char)(letter+13)
                : (char)(letter-13);
    }
}
