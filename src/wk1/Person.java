package wk1;

public class Person {
    public static void main(String[] args) {
        // https://docs.oracle.com/javase/specs/jls/se8/html/jls-5.html#jls-5.2
        // In addition, if the expression is a constant expression (§15.28) of type byte, short, char, or int:
        //
        //    A narrowing primitive conversion may be used if the type of the variable is
        //    byte, short, or char, and the value of the constant expression is representable
        //    in the type of the variable.
        System.out.println(Person.getNumberOfPeople());
        Person sarah = new Person("Sarah Jones", 120);
        System.out.println(Person.getNumberOfPeople());
        Person sally = new Person("Sally Jenkins", 150);
        System.out.println(Person.getNumberOfPeople());
        Person someone = sally;
        System.out.println(Person.getNumberOfPeople());
        System.out.println(sarah.toString());
        System.out.println(sally);
        System.out.println(someone);
        someone.setWeight(200);
        System.out.println(sally);
        System.out.println(someone);
        Person someoneElse = new Person();
        System.out.println(Person.getNumberOfPeople());
    }

    private static int count = 0;
    private final int id;
    private String name;
    private double weight;

    public Person(String name, double weight) {
        this.id = ++count;
        this.name = name;
        this.weight = weight;
    }

    public Person() {
        this("Jon Dough", 150);
    }

    public String toString() {
        return "Name: " + name + ", Weight: " + weight + " pounds";
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getNumberOfPeople() {
        return count;
    }
}
