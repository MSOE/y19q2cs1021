package wk1;

public class Rotator {
    public static final int DEFAULT_DISTANCE = 13;

    private final int distance;
    private final int rightShiftDistance;
    private final int leftShiftDistance;

    public Rotator() {
        this(DEFAULT_DISTANCE);
    }

    public Rotator(int distance) {
        this.distance = distance;
        rightShiftDistance = distance;
        leftShiftDistance = ('z' - 'a' + 1) - distance;
    }

    public String encrypt(String phrase) {
        StringBuilder result = new StringBuilder();
        for(int i=0; phrase!=null && i<phrase.length(); ++i) {
            result.append(rotateCharacter(phrase.charAt(i)));
        }
        return result.toString();
    }

    private char rotateCharacter(char character) {
        char result = character;
        if(Character.isLetter(character)) {
            if(shiftsRight(character)) {
                result += rightShiftDistance;
            } else {
                result -= leftShiftDistance;
            }
        }
        return result;
//        return !Character.isLetter(character) ? character
//                : shiftsRight(character) ? (char)(character+rightShiftDistance)
//                : (char)(character-leftShiftDistance);
    }

    private boolean shiftsRight(char letter) {
        boolean result = false;
        if(Character.isLowerCase(letter)) {
            result = Character.isLowerCase((char)(letter+ distance));
        } else {
            result = Character.isUpperCase((char)(letter+ distance));
        }
        return result;
//        char rotated = (char)(letter+distance);
//        return Character.isLowerCase(letter) ? Character.isLowerCase(rotated)
//                : Character.isUpperCase(rotated);
    }
}
