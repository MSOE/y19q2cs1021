package wk4;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class ExampleGUI extends Application {
    Stage stage;

    @Override
    public void start(Stage stage) {
        this.stage = stage;
        Pane pane = new FlowPane();
        Scene scene = new Scene(pane, 400, 400);
        TextField field = new TextField("This is text field");
        field.setOnAction(this::enterPressed);
        pane.getChildren().add(new Label("This is a label"));
        pane.getChildren().add(new Label("This is another label"));
        pane.getChildren().add(field);
        pane.getChildren().add(new Label("This is a label"));
        pane.getChildren().add(new Label("This is a label"));
        pane.getChildren().add(new Label("This is a label"));
        pane.getChildren().add(new Label("This is a label"));
        pane.getChildren().add(new Label("This is another label"));
        pane.getChildren().add(new Label("This is another label"));
        pane.getChildren().add(new Label("This is another label"));
        pane.getChildren().add(new Label("This is another label"));
        stage.setScene(scene);
        stage.show();
    }

    private void enterPressed(ActionEvent actionEvent) {
        TextField source = (TextField)actionEvent.getSource();
        System.out.println(source.getCharacters());
        boolean answer = source.getCharacters().toString().equalsIgnoreCase("full");
        stage.setFullScreen(answer);
        stage.show();
    }
}
