package wk4;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.Scanner;

public class Teenager extends Application {
    private Label response;
    public static final int HEIGHT = 200;
    public static final int WIDTH = 300;

    @Override
    public void start(Stage stage) {

        response = new Label();
        TextField input = new TextField();
        Button clear = new Button("Clear");

        Pane pane = new FlowPane();
        pane.getChildren().add(new Label("Please enter your age: "));
        pane.getChildren().add(input);
        pane.getChildren().add(response);
        pane.getChildren().add(clear);

        input.setOnAction(this::enterPressed);
        clear.setOnAction(this::clearPressed);

        stage.setScene(new Scene(pane, WIDTH, HEIGHT));
        stage.show();
    }

    private void clearPressed(ActionEvent actionEvent) {
        response.setText("");
    }

    private void enterPressed(ActionEvent actionEvent) {
        String text = ((TextField)actionEvent.getSource()).getText();
        int age = Integer.parseInt(text);
        String not = "";
        if(age<13 || age>19) {
            not = "not ";
        }
        response.setText("You are " + not + "a teenager.");
    }

    private void enterPressedRobust(ActionEvent actionEvent) {
        String text = ((TextField)actionEvent.getSource()).getText();
        Scanner parser = new Scanner(text);
        if(parser.hasNextInt()) {
            int age = parser.nextInt();
            boolean isTeenager = age>12 && age<20;
            response.setText("You are " + (isTeenager ? "" : "not ") + "a teenager");
        } else {
            response.setText("Please enter an integer");
        }
    }
}
